Distributed Systems Coursework
1353184

===Project Structure and Design Decisions===
The /source directory contains three projects, named 'StreamerX', where 'X' is one of the three parts of the coursework (A/B/C).
The /dependencies directory contains all the Ignite JARs needed to launch the nodes, which were obtained by including maven-dependency-plugin in pom.xml. This piece of code was removed once all dependencies were extracted.
Parts A and B are conceptually very similar to the provided examples. Part C attempts to avoid perpetual synchronization calls in the distributed cache during the streaming process by first aggregating the relevant data in a local HashMap, and afterwards streaming it to the Ignite cache. The query node can then identify if all streamers have finished, and finally pull data from the cache.

===Running 'compile'===
This calls the maven packaging command on each of the three projects, populating the /source/StreamerX/target directories, and finally placing the .jar files to the /build directory.

===Running 'partA'===
The script will launch nodes in the following order:

- The data node (in a new terminal window)
- The query node (in a new terminal window)
- The streamer node (in the current window; will exit Ignite upon reaching EOF)

===Running 'partB'===
The script will launch nodes in the following order:

- The data node (in a new terminal window)
- The query node (in a new terminal window)
- Two streamer nodes (in new terminal windows; will exit Ignite and close upon reaching EOF)
- The last streamer node (in the current window; will exit Ignite upon reaching EOF)

===Running 'partC'===
The script will launch nodes in the following order:

- The data node (in a new terminal window)
- The query node (in a new terminal window; will exit Ignite and close upon writing to log)
- Two streamer nodes (in new terminal windows; will exit Ignite and close upon reaching EOF)
- The last streamer node (in the current window; will exit Ignite upon reaching EOF)

Log files used for testing:
https://dumps.wikimedia.org/other/pagecounts-raw/2015/2015-08/pagecounts-20150802-000000.gz (used for part A)
https://dumps.wikimedia.org/other/pagecounts-raw/2016/2016-02/pagecounts-20160202-000000.gz
https://dumps.wikimedia.org/other/pagecounts-raw/2016/2016-08/pagecounts-20160802-000000.gz
