#!/bin/bash

# assume IGNITE_HOME is already set
export JAVA_HOME=/usr/lib/jvm/java-1.8.0

if [ "$1" = "compile" ]; then

	mvn -f ./source/StreamerA/pom.xml package
	mvn -f ./source/StreamerB/pom.xml package
	mvn -f ./source/StreamerC/pom.xml package

elif [ "$1" = "partA" ]; then

	gnome-terminal -e "$IGNITE_HOME/bin/ignite.sh examples/config/example-ignite-no-discovery.xml"
	sleep 5 # make sure data node has enough time to launch
	gnome-terminal -e "java -cp ./dependency/*:./build/streamer-a-1.0-SNAPSHOT.jar:. streamer.a.QueryWords"
	sleep 3 # make sure query node launches before streamer
	java -cp ./dependency/*:./build/streamer-a-1.0-SNAPSHOT.jar:. streamer.a.StreamWords "$2"

elif [ "$1" = "partB" ]; then

	gnome-terminal -e "$IGNITE_HOME/bin/ignite.sh examples/config/example-ignite-no-discovery.xml"
	sleep 5
	gnome-terminal -e "java -cp ./dependency/*:./build/streamer-b-1.0-SNAPSHOT.jar:. streamer.b.QueryWords"
	sleep 3
	gnome-terminal -e "java -cp ./dependency/*:./build/streamer-b-1.0-SNAPSHOT.jar:. streamer.b.StreamWords "$2""
	gnome-terminal -e "java -cp ./dependency/*:./build/streamer-b-1.0-SNAPSHOT.jar:. streamer.b.StreamWords "$3""
	java -cp ./dependency/*:./build/streamer-b-1.0-SNAPSHOT.jar:. streamer.b.StreamWords "$4"

elif [ "$1" = "partC" ]; then

	gnome-terminal -e "$IGNITE_HOME/bin/ignite.sh examples/config/example-ignite-no-discovery.xml"
	sleep 5
	gnome-terminal -e "java -cp ./dependency/*:./build/streamer-c-1.0-SNAPSHOT.jar:. streamer.c.QueryWords"
	sleep 3
	gnome-terminal -e "java -cp ./dependency/*:./build/streamer-c-1.0-SNAPSHOT.jar:. streamer.c.StreamWords "$2""
	gnome-terminal -e "java -cp ./dependency/*:./build/streamer-c-1.0-SNAPSHOT.jar:. streamer.c.StreamWords "$3""
	java -cp ./dependency/*:./build/streamer-c-1.0-SNAPSHOT.jar:. streamer.c.StreamWords "$4"
 
fi
