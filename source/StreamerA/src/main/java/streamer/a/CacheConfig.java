package streamer.a;

import org.apache.ignite.configuration.CacheConfiguration;

import javax.cache.configuration.FactoryBuilder;
import javax.cache.expiry.CreatedExpiryPolicy;
import javax.cache.expiry.Duration;
import static java.util.concurrent.TimeUnit.SECONDS;

public class CacheConfig {
    // Set up a cache storing the page name (String) and # of requests (Long)
    public static CacheConfiguration<String, Long> wordCache() {
        CacheConfiguration<String, Long> cfg = new CacheConfiguration<>("pages");

        // Set indexing to the same types as the cache entry
        cfg.setIndexedTypes(String.class, Long.class);

        // Set a sliding window of 1 second
        cfg.setExpiryPolicyFactory(FactoryBuilder.factoryOf(new CreatedExpiryPolicy(new Duration(SECONDS, 1))));

        return cfg;
    }
}