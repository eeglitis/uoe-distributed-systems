package streamer.a;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.query.SqlFieldsQuery;

import java.io.PrintWriter;
import java.util.List;

public class QueryWords {
    public static void main(String[] args) throws Exception {
        // Mark as client
        Ignition.setClientMode(true);

        // Create log file
        PrintWriter logWriter = new PrintWriter("log/log-partA.txt");

        try (Ignite ignite = Ignition.start("dependency/example-ignite-no-discovery.xml")) {
            // Identify the cache
            IgniteCache<String, Long> stmCache = ignite.getOrCreateCache(CacheConfig.wordCache());

            // Specify the query for the top 10 pages
            SqlFieldsQuery top10Qry = new SqlFieldsQuery("select _key, _val from Long order by _val desc limit 10");

            // Query the top10 perpetually
            while (true) {
                List<List<?>> top10 = stmCache.query(top10Qry).getAll();

                // Get the time of receiving the query
                Long posixTime = System.currentTimeMillis() / 1000L;

                // Print according to output specification
                top10.forEach(tuple -> {
                    String result = posixTime + ":" + tuple.get(0) + ":" + tuple.get(1);
                    logWriter.println(result);
                    System.out.println(result);
                });
                logWriter.flush();

                // Query every 10 seconds
                Thread.sleep(10000);
            }
        }
    }
}