package streamer.a;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteDataStreamer;
import org.apache.ignite.Ignition;
import org.apache.ignite.stream.StreamTransformer;

import javax.cache.processor.MutableEntry;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class StreamWords {
    public static void main(String[] args) throws Exception {
        // Mark as client
        Ignition.setClientMode(true);

        try (Ignite ignite = Ignition.start("dependency/example-ignite-no-discovery.xml")) {
            // Identify the cache
            IgniteCache<String, Long> stmCache = ignite.getOrCreateCache(CacheConfig.wordCache());

            // Make a streamer process and link it to the cache
            try (IgniteDataStreamer<String, Long> stmr = ignite.dataStreamer(stmCache.getName())) {

                // Set the updating policy (accumulating requests)
                stmr.allowOverwrite(true);
                stmr.receiver(new StreamTransformer<String, Long>() {
                    @Override public Object process(MutableEntry<String, Long> e, Object... args) {
                        Long val = e.getValue();
                        Long newVal = (Long) args[0];
                        e.setValue(val == null ? newVal : val + newVal);
                        return null;
                    }
                });

                Path path = Paths.get(args[0]);
                // Read line by line until end of file
                try (Stream<String> lines = Files.lines(path)) {
                    lines.forEach(line -> {
                        // Stream only name and access count to the cache
                        String[] words = line.split(" ");
                        stmr.addData(words[1], Long.parseLong(words[2]));
                    });
                }
            }
        }
    }
}