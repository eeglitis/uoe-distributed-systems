package streamer.b;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.query.SqlFieldsQuery;

import java.io.PrintWriter;
import java.util.List;

public class QueryWords {
    public static void main(String[] args) throws Exception {
        Ignition.setClientMode(true);
        PrintWriter logWriter = new PrintWriter("log/log-partB.txt");

        try (Ignite ignite = Ignition.start("dependency/example-ignite-no-discovery.xml")) {
            IgniteCache<String, Long> stmCache = ignite.getOrCreateCache(CacheConfig.wordCache());
            SqlFieldsQuery top10Qry = new SqlFieldsQuery("select _key, _val from Long order by _val desc limit 10");

            while (true) {
                List<List<?>> top10 = stmCache.query(top10Qry).getAll();
                Long posixTime = System.currentTimeMillis() / 1000L;

                top10.forEach(tuple -> {
                    String result = posixTime + ":" + tuple.get(0) + ":" + tuple.get(1);
                    logWriter.println(result);
                    System.out.println(result);
                });
                logWriter.flush();

                Thread.sleep(10000);
            }
        }
    }
}