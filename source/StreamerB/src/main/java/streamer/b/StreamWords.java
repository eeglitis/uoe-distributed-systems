package streamer.b;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteDataStreamer;
import org.apache.ignite.Ignition;
import org.apache.ignite.stream.StreamTransformer;

import javax.cache.processor.MutableEntry;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class StreamWords {
    public static void main(String[] args) throws Exception {
        Ignition.setClientMode(true);

        try (Ignite ignite = Ignition.start("dependency/example-ignite-no-discovery.xml")) {
            IgniteCache<String, Long> stmCache = ignite.getOrCreateCache(CacheConfig.wordCache());

            try (IgniteDataStreamer<String, Long> stmr = ignite.dataStreamer(stmCache.getName())) {
                stmr.allowOverwrite(true);
                stmr.receiver(new StreamTransformer<String, Long>() {
                    @Override public Object process(MutableEntry<String, Long> e, Object... args) {
                        Long val = e.getValue();
                        Long newVal = (Long) args[0];
                        e.setValue(val == null ? newVal : val + newVal);
                        return null;
                    }
                });

                Path path = Paths.get(args[0]);
                try (Stream<String> lines = Files.lines(path)) {
                    lines.forEach(line -> {
                        String[] words = line.split(" ");
                        stmr.addData(words[1], Long.parseLong(words[2]));
                    });
                }
            }
        }
    }
}