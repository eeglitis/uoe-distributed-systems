package streamer.c;

import org.apache.ignite.configuration.CacheConfiguration;

public class CacheConfig {
    public static CacheConfiguration<String, Long> wordCache() {
        CacheConfiguration<String, Long> cfg = new CacheConfiguration<>("pages");
        cfg.setIndexedTypes(String.class, Long.class);

        // No expiry policy needed

        return cfg;
    }
}
