package streamer.c;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteCluster;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.query.SqlFieldsQuery;

import java.io.PrintWriter;
import java.util.List;

public class QueryWords {
    public static void main(String[] args) throws Exception {
        Ignition.setClientMode(true);
        PrintWriter logWriter = new PrintWriter("log/log-partC.txt");

        try (Ignite ignite = Ignition.start("dependency/example-ignite-no-discovery.xml")) {
            IgniteCache<String, Long> stmCache = ignite.getOrCreateCache(CacheConfig.wordCache());
            SqlFieldsQuery top10Qry = new SqlFieldsQuery("select _key, _val from Long order by _val desc limit 10");

            // Set the cluster variable to track of node count
            // so that querying only happens when streamers are done
            IgniteCluster cluster = ignite.cluster();

            while (true) {
                List<List<?>> top10 = stmCache.query(top10Qry).getAll();
                // Only read once there are only 2 nodes remaining (cache and query)
                // and the cache contains data (to prevent exit during initial connecting phase)
                if ((cluster.metrics().getTotalNodes() == 2) && top10.size()>0) {
                    Long posixTime = System.currentTimeMillis() / 1000L;

                    top10.forEach(tuple -> {
                        String result = posixTime + ":" + tuple.get(0) + ":" + tuple.get(1);
                        logWriter.println(result);
                        System.out.println(result);
                    });
                    logWriter.flush();
                    break;
                }

                // Wait a bit to prevent too many queries
                Thread.sleep(1000);
            }
        }
    }
}
