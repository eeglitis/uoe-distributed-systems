package streamer.c;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteDataStreamer;
import org.apache.ignite.Ignition;
import org.apache.ignite.stream.StreamTransformer;

import javax.cache.processor.MutableEntry;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class StreamWords {
    public static void main(String[] args) throws Exception {
        // Aggregate data locally first, since this part does not need streaming
        // and hence can avoid perpetual cache consistency calculations
        Map<String, Long> map = new HashMap<>();

        Path path = Paths.get(args[0]);
        try (Stream<String> lines = Files.lines(path)) {
            lines.forEach(line -> {
                String[] words = line.split(" ");
                // Parse only words from Wikibooks
                if (words[0].endsWith(".b"))
                    // Increment map counts as needed
                    if (map.containsKey(words[1]))
                        map.replace(words[1], map.get(words[1])+Long.parseLong(words[2]));
                    else
                        map.put(words[1], Long.parseLong(words[2]));
            });
        }

        Ignition.setClientMode(true);

        try (Ignite ignite = Ignition.start("dependency/example-ignite-no-discovery.xml")) {
            IgniteCache<String, Long> stmCache = ignite.getOrCreateCache(CacheConfig.wordCache());

            try (IgniteDataStreamer<String, Long> stmr = ignite.dataStreamer(stmCache.getName())) {
                stmr.allowOverwrite(true);
                stmr.receiver(new StreamTransformer<String, Long>() {
                    @Override public Object process(MutableEntry<String, Long> e, Object... args) {
                        Long val = e.getValue();
                        Long newVal = (Long) args[0];
                        e.setValue(val == null ? newVal : val + newVal);
                        return null;
                    }
                });

                // Since file is finished, can stream all data to the cache
                map.forEach((k,v) -> stmr.addData(k,v));
            }
        }
    }
}